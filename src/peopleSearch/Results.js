import React, {Component} from 'react';
import SearchResult from './Result';
import loader from '../loader.gif';

export default class Results extends Component
{
    render() {

        // nothing to show
        if( this.props.queryState === false ) return '';

        // show loader
        if( this.props.queryState === 'doing_api_call' ) {

            return (
                <div className="loader">
                    <img src={loader} alt="loading" role="alert" />
                </div>
            );
        }

        // display query results
        if( this.props.queryState === 'query_complete' ) {

            // no results found
            if( this.props.results.length === 0 ) {

                return (
                    <div className="people-search-results">
                        <p className="people-search-no-results">No results Found</p>
                    </div>
                )
            }

            // list results
            return (
                <div className="people-search-results" data-testid="people-search-results">
                    {this.props.results.map((result, i) => <SearchResult key={i} resultCount={(i+1)} result={result} />)}
                </div>
            )
        }

        return '';
    }
}

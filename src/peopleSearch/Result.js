import React, {Component} from 'react';
export default class Result extends Component
{
    render() {

        const result = this.props.result;
        
        return (
            <div className="people-search-result">
                <ul className="people-search-result-data">
                    <li>Result #{this.props.resultCount}</li>
                    <li>FirstName: {result.FirstName}</li>
                    <li>LastName: {result.LastName}</li>
                    <li>Address: {result.Address}</li>
                    <li>City: {result.City}</li>
                    <li>State: {result.State}</li>
                </ul>
            </div>
        )
    }
}

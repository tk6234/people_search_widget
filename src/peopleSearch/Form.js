import React, {Component} from 'react';
import axios from 'axios';
import {uniqueEntries, peopleSearchUrl} from '../utils'

export default class Form extends Component
{
    constructor(props){
        super(props);

        this.firstName  = React.createRef();
        this.lastName   = React.createRef();
        this.stateInput = React.createRef();

        this.state = {errors: []}

        this.datafinderQuery = this.datafinderQuery.bind(this);
        this.handleSubmit    = this.handleSubmit.bind(this);
    }

    datafinderQuery() {

        axios.get( peopleSearchUrl(this.firstName.current.value, this.lastName.current.value, this.stateInput.current.value) )
        .then(res => {
            const data = res.data;

            if( typeof data.datafinder != 'undefined' ) {
                this.props.setSearchResults( uniqueEntries(data.datafinder.results, this.stateInput.current.value) );
                this.props.setQueryState('query_complete');
            }
        });
    }

    handleSubmit(e) {
        e.preventDefault();

        if( this.isValidSubmission() ) {
            this.props.setQueryState('doing_api_call');
            this.datafinderQuery();
        }
    }

    isValidSubmission() {

        var errors = [];

        if( ! this.firstName.current.value ) { errors.push('firstName') }
        if( ! this.lastName.current.value ) { errors.push('lastName') }

        this.setState({errors: errors});

        return (errors.length === 0);
    }

    getInputClassName(inputName) {

        var className = 'people-search-form-input-field';

        if( this.state.errors.includes(inputName) ) {
            className += ' error';
        }

        return className;
    }

    isDisabled() {
        return (this.props.queryState === 'doing_api_call');
    }

    render() {
        return (
            <form className="people-search-form" onSubmit={this.handleSubmit}>
                <div className={this.getInputClassName('firstName')} data-testid="people-search-firstName">
                    <label>FirstName</label>
                    <input type="text" aria-label="first-name" ref={this.firstName} placeholder="eg. John" disabled = {this.isDisabled()} />
                </div>
                <div className={this.getInputClassName('lastName')} data-testid="people-search-lastName">
                    <label>LastName</label>
                    <input type="text" aria-label="last-name" ref={this.lastName} placeholder="eg. Smith" disabled = {this.isDisabled()} />
                </div>
                <div className={this.getInputClassName('stateInput')} data-testid="people-search-stateInput">
                    <label>State</label>
                    <input className="transform-uppercase" aria-label="state" type="text" ref={this.stateInput} maxLength="2" placeholder="eg. LA" disabled = {this.isDisabled()} />
                </div>
                <div className="people-search-form-input-field">
                    <button type="submit" aria-label="submit" className="btn btn-primary" disabled = {this.isDisabled()}>Submit</button>
                </div>
            </form>
        )
    }
}

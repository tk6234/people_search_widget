import React, {Component} from 'react';
import SearchForm from './Form';
import SearchResults from './Results';
import './styles.css';

export default class Widget extends Component
{

    constructor(props) {
        super(props)
        this.state = {
            results: null,
            queryState: false,
        }
    }

    setSearchResults = (persons) => {
        this.setState({results: persons});
    }

    setQueryState = (state) => {
        this.setState({queryState: state});
    }

    render() {
        return (
            <div className="people-search">
                <SearchForm setSearchResults={this.setSearchResults} setQueryState={this.setQueryState} queryState={this.state.queryState} />
                <SearchResults results={this.state.results} queryState={this.state.queryState} />
            </div>
        )
    }
}

import React from 'react';
import Header from './layout/Header';
import PeopleSearch from './peopleSearch/Widget';

import './App.css';

function App() {
  return (
    <div className="App">
        <Header />
        <PeopleSearch />
    </div>
  );
}

export default App;

import React from 'react';
import { peopleSearchUrl } from './utils';
import responseData from './data/responseData';
import { render, fireEvent, wait } from '@testing-library/react';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import App from './App';

test('versium people search widget', async () => {

    const { container, getByLabelText, getByRole, getByTestId, queryAllByText } = render(<App />);
    const firstName   = getByLabelText('first-name');
    const lastName    = getByLabelText('last-name');
    const state       = getByLabelText('state');
    const submit      = getByLabelText('submit');

    var mock   = new MockAdapter(axios);
    mock.onGet( peopleSearchUrl('hank', 'hill', 'tx') ).reply(200, responseData);

    // submit form
    submit.click(submit)

    // test validation
    await wait(() => {
        expect(getByTestId('people-search-firstName')).toHaveClass('error')
        expect(getByTestId('people-search-lastName')).toHaveClass('error')
    })

    // set form fields
    fireEvent.change(firstName, { target: { value: 'hank' } })
    fireEvent.change(lastName, { target: { value: 'hill' } })
    fireEvent.change(state, { target: { value: 'tx' } })

    // submit form
    submit.click(submit)

    // test loader
    expect(getByRole('alert')).toBeInTheDocument()

    // confirm we have results
    await wait(() => {
        var results = queryAllByText(/Result #/i);
        expect(results).toHaveLength(52)
    })

});

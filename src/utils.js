import hasher from 'object-hash';

/**
 * Removes duplicate entries based on a person's name and address
 *
 * @param {Array} data
 *
 * @return {Array}
 */
const uniqueEntries = function(data, state) {

    if( ! data ) return [];

    var results = [];
    var hashes  = {};

    for(var i=0; i<data.length; i++) {
        var result = data[i];

        if( state && result.State ) {

            if( state.toLowerCase() !== result.State.toLowerCase() ) {
                continue;
            }
        }

        var key = hasher({
            FirstName: result.FirstName,
            LastName: result.LastName,
            Address: result.Address,
            City: result.City,
            State: result.State,
        });

        if( typeof hashes[key] === 'undefined' ) {
            hashes[key] = 1;
            results.push(data[i]);
        }
    }

    return results;
}

/**
 * Get datafinder api url
 *
 * @param {String} firstName
 * @param {String} lastName
 * @param {String} state
 *
 * @return {String}
 */
const peopleSearchUrl = function(firstName, lastName, state) {

    var proxy = 'https://cors-anywhere.herokuapp.com/';
    var url   = `https://api.datafinder.com/qdf.php?service=phone&k2=9abbxna7d2b65ivia3p9vljs&cfg_maxrecs=100&d_first=${firstName}&d_last=${lastName}&d_state=${state}`;

    return proxy + url;
}

export {
    uniqueEntries,
    peopleSearchUrl
}
